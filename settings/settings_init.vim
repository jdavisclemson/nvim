
let g:lists_modifiable = 1
let g:popup_transparency = 20
let g:coc_enable_locationlist = 0
let window_resize_increment = 10
let &t_SI="\e[6 q"
let &t_EI="\e[2 q"
let g:tagbar_highlight_method = 'nearest'
let g:coc_suggest_blacklist = []
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "="]
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["[", "{", "]", "}", "\\", "|", ";", ":", "'", "\"", ",", "<", ".", ">", "/", "?"]
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["-0", "-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9"]
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["+0", "+1", "+2", "+3", "+4", "+5", "+6", "+7", "+8", "+9"]
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
let g:coc_suggest_blacklist = g:coc_suggest_blacklist + ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
let g:lightline = {
	\'colorscheme': 'edge',
	\'active': {
		\'left': [ [ 'filename', 'tag_information' ], ]
	\},
	\'component_function': {
		\'tag_information': 'TagInformation',
	\},
	\'enable': {
		\'tabline': 0
	\},
\}
colorscheme edge
filetype plugin on
filetype plugin indent on
syntax on
set background=dark
set confirm
set termguicolors
set notimeout
set nottimeout
set nowrap
set noshowmatch
set ignorecase
set incsearch
set tabstop=4
set softtabstop=0
set noexpandtab
set shiftwidth=4
set noautoindent
set nosmarttab
set number norelativenumber
set wildmode=longest,list
set colorcolumn=0
set mouse=a
set cursorline
set nospell
set noswapfile
set nobackup
set whichwrap+=<,>
set whichwrap+=[,]
set nohlsearch
set linebreak
set shortmess+=at
set shortmess+=I
set listchars=tab:>·
set nolist
set updatetime=500
set textwidth=0
"The setting below only takes effect when textwidth is zero.
set wrapmargin=0
set noautoread
set cmdheight=2
set noshowcmd
set list
set foldlevel=99
exe "set pumblend=" .. g:popup_transparency

