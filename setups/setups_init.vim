lua << EOF
	initvimdir=os.getenv("MYVIMRC"):gsub("init.vim","")

	require'nvim-treesitter.configs'.setup {
		textobjects = {
			move = {
				enable = true,
				set_jumps = false, -- whether to set jumps in the jumplist
				goto_next_start = {
					["]]"] = "@block.outer",
					["]f"] = "@function.outer",
					["]m"] = "@function.outer",
					["]c"] = "@class.outer",
				},
				goto_next_end = {
					["]}"] = "@block.outer",
					["]F"] = "@function.outer",
					["]M"] = "@function.outer",
					["]C"] = "@class.outer",
				},
				goto_previous_start = {
					["[["] = "@block.outer",
					["[f"] = "@function.outer",
					["[m"] = "@function.outer",
					["[c"] = "@class.outer",
				},
				goto_previous_end = {
					["[{"] = "@block.outer",
					["[F"] = "@function.outer",
					["[M"] = "@function.outer",
					["[C"] = "@class.outer",
				},
			},
		},
	}

	dofile(initvimdir .. "setups/neo-tree_setup.lua")
	dofile(initvimdir .. "setups/bufferline_setup.lua")
	local dap = require('dap')
	--Below is a starting place for bash debug adapater configuration.
	--	This will require installing bashdb.
	--local bash_debug = vim.fn.glob('~/Downloads/bash-debug/extension')
	--dap.adapters.bashdb = {
	--	  type = 'executable',
	--	  command = 'node',
	--	  args = { bash_debug..'/out/bashDebug.js' }
	--}
	--dap.configurations.sh = {
	--	  {
	--		  request = 'launch',
	--		  type = 'bashdb',
	--		  program = '${file}',
	--		  args = {},
	--		  env = {},
	--		  pathBash = 'bash',
	--		  pathBashdb = bash_debug..'/bashdb_dir/bashdb',
	--		  pathBashdbLib = bash_debug..'/bashdb_dir',
	--		  pathCat = 'cat',
	--		  pathMkfifo = 'mkfifo',
	--		  pathPkill = 'pkill',
	--		  cwd = '${workspaceFolder}',
	--		  terminalKind = 'integrated'
	--	  }
	--}
	--This is a starting point for getting lua debugging to work
	--dap.adapters.nlua = function(callback, config)
	--	callback({ type = 'server', host = config.host, port = config.port })
	--end
	--
	--dap.configurations.lua = { 
	--	{ 
	--	  type = 'nlua', 
	--	  request = 'attach',
	--	  name = "Attach to running Neovim instance",
	--	  host = function()
	--		local value = vim.fn.input('Host [127.0.0.1]: ')
	--		if value ~= "" then
	--		  return value
	--		end
	--		return '127.0.0.1'
	--	  end,
	--	  port = function()
	--		local val = tonumber(vim.fn.input('Port: '))
	--		assert(val, "Please provide a port number")
	--		return val
	--	  end,
	--	}
	--}
	dap.adapters.codelldb = function(callback, _)
		local command = 'codelldb --port 13000'
		vim.cmd('vsp term://' .. vim.o.shell)
		local bufnr = vim.api.nvim_get_current_buf()
		pcall(vim.api.nvim_chan_send, vim.b.terminal_job_id, command .. '\n')
		vim.api.nvim_buf_set_name(0, "codelldb_server")
		vim.defer_fn(function()
			callback({type = 'server', host = '127.0.0.1', port = 13000})
		end, 1000)
		vim.cmd [[ :q ]]
	end
	dap.configurations.cpp = {
		{
			type = "codelldb",
			request = "launch",
			cwd = '${workspaceFolder}',
			console = 'integratedTerminal',
			stopOnEntry = false,
			program = function()
				return vim.fn.input('executable: ', vim.fn.getcwd() .. '/', 'file')
			end
		}
	}
	require("dap-python").setup("python",{})
	dofile(initvimdir .. "setups/dapui_setup.lua")
	dofile(initvimdir .. "setups/nvim-dap-virtual-text_setup.lua")
	require('telescope').load_extension('dap')
	dap.listeners.before['event_initialized']['dapui'] = function(session, body)
		require'dapui'.close()
		vim.api.nvim_command(':call StoreCurrentBuffer()')
	end
	dap.listeners.after.event_initialized["dapui_config"] = function()
		vim.api.nvim_command(':NeoTreeClose')
		vim.api.nvim_command(':TagbarClose')
		require'dapui'.open()
		vim.api.nvim_command(':call MoveToEdge("h")')
		vim.api.nvim_command(':vertical resize 50')
		vim.api.nvim_command(':wincmd l')
	end
	dap.listeners.before.event_terminated["dapui_config"] = function()
		vim.api.nvim_command(':call SwitchToStoredBuffer()')
	end
	--dap.listeners.before.event_exited["dapui_config"] = function()
	--	require'dapui'.close()
	--end
	dap.listeners.after.event_exited["dapui_config"] = function()
		vim.api.nvim_command(':call CleanDapBuffers()')
	end

EOF
