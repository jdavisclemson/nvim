
execute "source " .. expand(g:rootdir .. "/mappings/no-op_mappings.vim")

noremap <Down> gj
noremap <Up> gk
noremap <Leader>b :lua require'dap'.toggle_breakpoint()<CR>
noremap <Leader>B :call BreakPoints()<CR>
noremap <Leader>k :call ToggleComment()<CR>
noremap <Leader>K :call UnComment()<CR>
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk
inoremap <Esc> <Esc>`^
nnoremap <C-k> :wincmd k<CR>
nnoremap <C-j> :wincmd j<CR>
nnoremap <C-h> :wincmd h<CR>
nnoremap <C-l> :wincmd l<CR>
nnoremap <C-Up> :wincmd k<CR>
nnoremap <C-Down> :wincmd j<CR>
nnoremap <C-Left> :wincmd h<CR>
nnoremap <C-Right> :wincmd l<CR>
inoremap <silent><expr> <tab> coc#pum#visible() ? coc#pum#confirm() : "\<tab>"
inoremap <silent><expr> <C-n> coc#pum#visible() ? coc#pum#next(1) : "\<C-n>"
inoremap <silent><expr> <C-p> coc#pum#visible() ? coc#pum#prev(1) : "\<C-p>"
inoremap <silent><expr> <C-j> coc#pum#visible() ? coc#pum#next(1) : "\<C-j>"
inoremap <silent><expr> <C-k> coc#pum#visible() ? coc#pum#prev(1) : "\<C-k>"
inoremap <silent><expr> <PageDown> coc#pum#visible() ? coc#pum#scroll(1) : "\<PageDown>"
inoremap <silent><expr> <PageUp> coc#pum#visible() ? coc#pum#scroll(0) : "\<PageUp>"
inoremap <silent><expr> <S-Down> coc#pum#visible() ? coc#pum#scroll(1) : "\<S-Down>"
inoremap <silent><expr> <S-Up> coc#pum#visible() ? coc#pum#scroll(0) : "\<S-Up>"
inoremap <expr> ) CloseParentheses()
nnoremap <Leader>t :NeoTreeShowToggle<CR>
nnoremap <Tab> :BufferLineCycleNext<CR>
nnoremap <S-tab> :BufferLineCyclePrev<CR>
nnoremap gt :BufferLineCycleNext<CR>
nnoremap gT :BufferLineCyclePrev<CR>
nnoremap <A-k> :call MoveToEdge("k")<CR>
nnoremap <A-j> :call MoveToEdge("j")<CR>
nnoremap <A-h> :call MoveToEdge("h")<CR>
nnoremap <A-l> :call MoveToEdge("l")<CR>
nnoremap <Leader>p "+p
nnoremap <Leader>P "+P
inoremap <C-v> <C-r>0
"inoremap <BS> <Left><DEL>
"inoremap <CR> <C-R>=NormalEnter()<CR>
nnoremap <F5> :lua require'dap'.continue()<CR>
nnoremap <Leader><F5> :call DapClose()<CR>
nnoremap <Leader>2 @:
xnoremap <Leader>2 @:
nnoremap <Leader>n :call SearchNext(0)<CR>
nnoremap <Leader>N :call SearchNext(1)<CR>
xnoremap <Leader>y :call YankToSystemRegister("x")<CR>
nnoremap <Leader>y :call YankToSystemRegister("n")<CR>
nnoremap <Leader>p "+p
nnoremap <Leader>P "+P
nnoremap <Leader>f :call FindAllInFile("n", "\\c")<CR>
nnoremap <Leader>r :call FindAllInRoot("n", "\\c")<CR>
xnoremap <Leader>f :call FindAllInFile("x", "\\c")<CR>
xnoremap <Leader>r :call FindAllInRoot("x", "\\c")<CR>
nnoremap <Leader>F :call FindAllInFile("n", "\\C")<CR>
nnoremap <Leader>R :call FindAllInRoot("n", "\\C")<CR>
xnoremap <Leader>F :call FindAllInFile("x", "\\C")<CR>
xnoremap <Leader>R :call FindAllInRoot("x", "\\C")<CR>
nnoremap <Leader>c :wincmd c<CR>
nnoremap <Leader>C :call CloseWindow()<CR>
nnoremap <Leader>o :TagbarToggle<CR>
nnoremap <Leader>v :vsplit \| wincmd l<CR>
nnoremap <expr> <Leader>h WindowIsLocationList() ? ":call SearchAndReplace()\<CR>" : ":split \\| wincmd j\<CR>"
nnoremap <Leader>q :Bdelete<CR>
nnoremap <Leader>Q :Bdelete!<CR>
nnoremap <Leader>K :call CommentDuplicate()<CR>
nnoremap <C-s> :w<CR>
tnoremap <Esc> <C-\><C-n>
xnoremap <expr> <cr> IsWindowNeotree() ? ":call OpenAllFiles()\<cr>" : "\<cr>"
nnoremap ]] ]m
nnoremap [[ [m
xnoremap <Leader>s :call SurroundText()<CR>
nnoremap <Leader>s WBvE:call SurroundText()<CR>
nnoremap <Leader>w wbve
nnoremap <Leader>W WBvE
xnoremap > >gv
xnoremap < <gv
nmap <expr> <F2> IsWindowNeotree() ? "r" : "\<F2>"
