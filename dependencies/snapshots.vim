
"silent! let g:plugs['FastFold'].commit = '542683b0cce738be22b6fdadb08302faed68e7b4'
"silent! let g:plugs['bufferline.nvim'].commit = '028a87933d99f8bb88f2f70a4def3ff9574f3594'
"silent! let g:plugs['coc.nvim'].commit = '54ae03ed6e0a5973464beffb0064fcee4cf730ed'
"silent! let g:plugs['edge'].commit = '2c8026cd5b1eaca890739799dc57ca8d3ca733b0'
"silent! let g:plugs['lightline.vim'].commit = 'b1e91b41f5028d65fa3d31a425ff21591d5d957f'
"silent! let g:plugs['neo-tree.nvim'].commit = 'ab8ca9fac52949d7a741b538c5d9c3898cd0f45a'
"silent! let g:plugs['nui.nvim'].commit = 'd12a6977846b2fa978bff89b439e509320854e10'
"silent! let g:plugs['nvim-dap'].commit = '61643680dcb771a29073cd432894e2f81a7c2ae3'
"silent! let g:plugs['nvim-dap-python'].commit = '27a0eff2bd3114269bb010d895b179e667e712bd'
"silent! let g:plugs['nvim-dap-ui'].commit = 'f7fc98ead677ffed72d4eec331eb439a7bad3bbf'
"silent! let g:plugs['nvim-dap-virtual-text'].commit = '2971ce3e89b1711cc26e27f73d3f854b559a77d4'
"silent! let g:plugs['nvim-treesitter'].commit = '18cc1216e128b2db1046cf7b3a46851c7c4e4073'
"silent! let g:plugs['nvim-treesitter-textobjects'].commit = 'f27f22053d210191e0a267ca15ec80a10a226a97'
"silent! let g:plugs['nvim-web-devicons'].commit = '3b1b794bc17b7ac3df3ae471f1c18f18d1a0f958'
"silent! let g:plugs['plenary.nvim'].commit = '4b7e52044bbb84242158d977a50c4cbcd85070c7'
"silent! let g:plugs['tagbar'].commit = '6c3e15ea4a1ef9619c248c2b1eced56a47b61a9e'
"silent! let g:plugs['telescope-dap.nvim'].commit = 'b4134fff5cbaf3b876e6011212ed60646e56f060'
"silent! let g:plugs['telescope.nvim'].commit = 'cea9c75c19d172d2c6f089f21656019734a615cf'
"silent! let g:plugs['vim-bbye'].commit = '25ef93ac5a87526111f43e5110675032dbcacf56'
"silent! let g:plugs['vim-fugitive'].commit = '49cc58573e746d02024110d9af99e95994ea4b72'

"silent! let g:plugs['FastFold'].commit = 'ab3d199d288a51708c3181a25aba1f9de2050b89'
"silent! let g:plugs['bufferline.nvim'].commit = '41660189da6951d14436147dff30ed7f0d12ed01'
"silent! let g:plugs['coc.nvim'].commit = 'bbaa1d5d1ff3cbd9d26bb37cfda1a990494c4043'
"silent! let g:plugs['edge'].commit = '358cb6688ac577470a4eafcb53bdd63899dfc937'
"silent! let g:plugs['lightline.vim'].commit = 'b1e91b41f5028d65fa3d31a425ff21591d5d957f'
"silent! let g:plugs['neo-tree.nvim'].commit = 'd883632bf8f92f1d5abea4a9c28fb2f90aa795aa'
"silent! let g:plugs['nui.nvim'].commit = '64bdc579873fa5bd303f6951ead2b419493c88e8'
"silent! let g:plugs['nvim-dap'].commit = '7c1d47cf7188fc31acdf951f9eee22da9d479152'
"silent! let g:plugs['nvim-dap-python'].commit = '37b4cba02e337a95cb62ad1609b3d1dccb2e5d42'
"silent! let g:plugs['nvim-dap-ui'].commit = 'c020f660b02772f9f3d11f599fefad3268628a9e'
"silent! let g:plugs['nvim-dap-virtual-text'].commit = '57f1dbd0458dd84a286b27768c142e1567f3ce3b'
"silent! let g:plugs['nvim-treesitter'].commit = 'f9d701176cb9a3e206a4c690920a8993630c3ec8'
"silent! let g:plugs['nvim-treesitter-textobjects'].commit = '2d6d3c7e49a24f6ffbbf7898241fefe9784f61bd'
"silent! let g:plugs['nvim-web-devicons'].commit = '2a125024a137677930efcfdf720f205504c97268'
"silent! let g:plugs['plenary.nvim'].commit = '36aaceb6e93addd20b1b18f94d86aecc552f30c4'
"silent! let g:plugs['tagbar'].commit = 'be563539754b7af22bbe842ef217d4463f73468c'
"silent! let g:plugs['telescope-dap.nvim'].commit = '313d2ea12ae59a1ca51b62bf01fc941a983d9c9c'
"silent! let g:plugs['telescope.nvim'].commit = '89ca7265726cb07ca316562b13fd0c406588af65'
"silent! let g:plugs['vim-bbye'].commit = '25ef93ac5a87526111f43e5110675032dbcacf56'
"silent! let g:plugs['vim-fugitive'].commit = '5f0d280b517cacb16f59316659966c7ca5e2bea2'

"nvim-treesitter, nvim-treesitter-textobjects, and bufferline.nvim updated below...

silent! let g:plugs['FastFold'].commit = 'ab3d199d288a51708c3181a25aba1f9de2050b89'
silent! let g:plugs['bufferline.nvim'].commit = '5c528bee3dd797d5bd6bae5f229411939b25b203'
silent! let g:plugs['coc.nvim'].commit = 'bbaa1d5d1ff3cbd9d26bb37cfda1a990494c4043'
silent! let g:plugs['edge'].commit = '358cb6688ac577470a4eafcb53bdd63899dfc937'
silent! let g:plugs['lightline.vim'].commit = 'b1e91b41f5028d65fa3d31a425ff21591d5d957f'
silent! let g:plugs['neo-tree.nvim'].commit = 'd883632bf8f92f1d5abea4a9c28fb2f90aa795aa'
silent! let g:plugs['nui.nvim'].commit = '64bdc579873fa5bd303f6951ead2b419493c88e8'
silent! let g:plugs['nvim-dap'].commit = '7c1d47cf7188fc31acdf951f9eee22da9d479152'
silent! let g:plugs['nvim-dap-python'].commit = '37b4cba02e337a95cb62ad1609b3d1dccb2e5d42'
silent! let g:plugs['nvim-dap-ui'].commit = 'c020f660b02772f9f3d11f599fefad3268628a9e'
silent! let g:plugs['nvim-dap-virtual-text'].commit = '57f1dbd0458dd84a286b27768c142e1567f3ce3b'
silent! let g:plugs['nvim-treesitter'].commit = '92d2501d698e0fe855bd222540f9648890fab6c7'
silent! let g:plugs['nvim-treesitter-textobjects'].commit = '3e450cd85243da99dc23ebbf14f9c70e9a0c26a4'
silent! let g:plugs['nvim-web-devicons'].commit = '2a125024a137677930efcfdf720f205504c97268'
silent! let g:plugs['plenary.nvim'].commit = '36aaceb6e93addd20b1b18f94d86aecc552f30c4'
silent! let g:plugs['tagbar'].commit = 'be563539754b7af22bbe842ef217d4463f73468c'
silent! let g:plugs['telescope-dap.nvim'].commit = '313d2ea12ae59a1ca51b62bf01fc941a983d9c9c'
silent! let g:plugs['telescope.nvim'].commit = '89ca7265726cb07ca316562b13fd0c406588af65'
silent! let g:plugs['vim-bbye'].commit = '25ef93ac5a87526111f43e5110675032dbcacf56'
silent! let g:plugs['vim-fugitive'].commit = '5f0d280b517cacb16f59316659966c7ca5e2bea2'
