
"Required dependencies:
"let g:required_shell_commands = ['python', 'pip', 'node', 'npm', 'ctags', 'codelldb', 'xclip']
let g:required_shell_commands = ['python', 'pip', 'node', 'npm', 'ctags', 'xclip']
let g:required_python_packages = ['neovim', 'pynvim', 'debugpy']
let g:required_coc_extensions = ['coc-pyright', 'coc-json', 'coc-vimlsp', 'coc-lua', 'coc-clangd', 'coc-sh']
let g:required_parsers = ['python', 'json', 'cpp', 'lua', 'bash']

"Check for required shell commands.
let missing_shell_commands = filter(g:required_shell_commands, '!executable(v:val)')
if len(missing_shell_commands) > 0
	let msg = "Error: The following shell commands are missing:"
	for shcmd in missing_shell_commands
		let msg = msg .. " " .. shcmd
	endfor
	let msg = msg .. ". Please install them."
	echom(msg)
	let g:stop_execution = 1
	finish
endif

"If any python packages are missing, prompt the user to install them.
let pip_freeze_output = systemlist('pip freeze')
let installed_python_packages = []
for pypkg in pip_freeze_output
	call add(installed_python_packages, split(pypkg,"==")[0])
endfor
let missing_python_packages = filter(g:required_python_packages, 'index(installed_python_packages, v:val) == -1')
if len(missing_python_packages) > 0
	let msg = "The following python packages are missing:"
	for pypkg in missing_python_packages
		let msg = msg .. " " .. pypkg
	endfor
	let msg = msg .. ". Would you like to install them? (y/N): "
	let response = input(msg)
	if tolower(response) == "y"
		for pypkg in missing_python_packages
			call system("pip install " .. pypkg)
		endfor
	else
		let g:stop_execution = 1
		finish
	endif
endif

"If vim-plug is not installed, prompt the user to install it.
let vim_plug_path = stdpath('data') .. "/site/autoload/plug.vim"
if empty(glob(vim_plug_path))
	let msg = "Vim-plug is missing. Would you like to install it now? (y/N): "
	let response = input(msg)
	if tolower(response) == "y"
		exe "silent !curl -fLo " .. vim_plug_path .. " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
	else
		let g:stop_execution = 1
		finish
	endif
endif

call plug#begin()
	"Make sure python and pip are installed.
	"  On some platforms (WSL - Ubuntu is an example), you need to make sure that python3, pip3, and python-is-python3 is installed.
	Plug 'https://github.com/sainnhe/edge.git'
	Plug 'https://github.com/akinsho/bufferline.nvim', { 'tag': '*' }
	Plug 'https://github.com/kyazdani42/nvim-web-devicons'
	"Note that the terminal font must be set to a Nerd Font.
	"  This may require installing a font.
	Plug 'https://github.com/nvim-neo-tree/neo-tree.nvim'
	"Note that neo-tree requires plenary.nvim, nvim-web-devicons, and nui.vim.
	Plug 'https://github.com/MunifTanjim/nui.nvim'
	Plug 'https://github.com/neoclide/coc.nvim', {'branch': 'release'}
	"Requires nodejs and npm.
	"Requires python packages neovim and pynvim.
	"Once this plugin is installed, use :CocInstall to install the lsp packages you want.
	"  At the time of writing, I'm using coc-pyright, coc-json, coc-vimlsp, coc-lua, coc-clangd, coc-sh.
	"	 Other python lsp options include coc-pylsp and coc-jedi.
	Plug 'https://github.com/mfussenegger/nvim-dap'
	"Requires codelldb for C++.
	"Requires python package debugpy.
	Plug 'https://github.com/mfussenegger/nvim-dap-python'
	Plug 'https://github.com/nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	"Once nvim-treesitter is installed, you need to :TSInstall python and :TSInstall cpp.
	Plug 'https://github.com/rcarriga/nvim-dap-ui'
	Plug 'https://github.com/theHamsta/nvim-dap-virtual-text'
	Plug 'https://github.com/nvim-lua/plenary.nvim'
	Plug 'https://github.com/nvim-telescope/telescope.nvim'
	Plug 'https://github.com/nvim-telescope/telescope-dap.nvim'
	Plug 'https://github.com/moll/vim-bbye'
	Plug 'https://github.com/preservim/tagbar'
	"TagBar requires a tags client to be installed (universal ctags is recommended).
	Plug 'https://github.com/itchyny/lightline.vim'
	Plug 'https://github.com/Konfekt/FastFold'
	Plug 'https://github.com/nvim-treesitter/nvim-treesitter-textobjects'
	Plug 'https://github.com/tpope/vim-fugitive'
call plug#end()


"If any plugins are missing, prompt the user to install them.
let missing_plugins = filter(keys(g:plugs), '!isdirectory(g:plugs[v:val].dir)')
if len(missing_plugins) > 0
	let msg = "The following neovim plugins are missing:"
	for plugin in missing_plugins
		let msg = msg .. " " .. plugin
	endfor
	let msg = msg .. ". Would you like to install them now? (y/N): "
	let response = input(msg)
	if tolower(response) == "y"
		execute "source " .. expand(g:rootdir .. "/dependencies/snapshots.vim")
		PlugInstall --sync
	else
		let g:stop_execution = 1
		finish
	endif
endif

function! VerifyParsers()
	redir => rawTSInstallInfoOutput
		silent exe "TSInstallInfo"
	redir end
	let TSInstallInfoOutput = split(rawTSInstallInfoOutput, "\n")
	let installed_parsers = []
	for parser in TSInstallInfoOutput
		if stridx(parser, "installed") > -1 && stridx(parser, "not installed") == -1
			call add(installed_parsers, split(parser, " ")[0])
		endif
	endfor
	let missing_parsers = filter(g:required_parsers, 'index(installed_parsers, v:val) == -1')
	if len(missing_parsers) > 0
		let msg = "The following parsers are missing:"
		let cmd = "TSInstall"
		for parser in missing_parsers
			let msg = msg .. " " .. parser
			let cmd = cmd .. " " .. parser
		endfor
		let msg = msg .. ". Would you like to install them now? (y/N): "
		let response = input(msg)
		if tolower(response) == "y"
			exe cmd
		endif
	endif
endfunction

function! VerifyCocExtensions()
	let rawCocExtensionList = CocAction('extensionStats')
	let installed_coc_extensions = []
	for CocExtensionDict in rawCocExtensionList
		let CocExtensionName = CocExtensionDict['id']
		call add(installed_coc_extensions, CocExtensionName)
	endfor
	let missing_coc_extensions = filter(g:required_coc_extensions, 'index(installed_coc_extensions, v:val) == -1')
	if len(missing_coc_extensions) > 0
		let msg = "The following coc extensions are missing:"
		let cmd = "CocInstall"
		for cocext in missing_coc_extensions
			let msg = msg .. " " .. cocext
			let cmd = cmd .. " " .. cocext
		endfor
		let msg = msg .. ". Would you like to install them now? (y/N): "
		let response = input(msg)
		if tolower(response) == "y"
			exe cmd
		endif
	endif
endfunction

helptags ALL

