
if has("autocmd")
	augroup autocmds
		autocmd!
		autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
		" When editing a file, always jump to the last known cursor position.
		" Don't do it when the position is invalid or when inside an event handler
		" (happens when dropping a file on gvim).
		" Also don't do it when the mark is in the first line, that is the default
		" position when opening a file.
		autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
		autocmd CursorHold * silent call CocActionAsync('highlight')
		autocmd FileType * silent set formatoptions-=c
		autocmd FileType * silent set noexpandtab
		autocmd BufEnter * silent :cabclear | call CreateAbbreviations(g:global_cabbrevs)
		autocmd FileType c,cpp,java,arduino let b:comment_leader = '//'
		autocmd FileType sh,ruby,python,zsh,conf,fstab let b:comment_leader = '#'
		autocmd FileType matlab,tex let b:comment_leader = '%'
		autocmd FileType vim let b:comment_leader = '"'
		autocmd FileType lua let b:comment_leader = '--'
		autocmd FileType python set foldmethod=expr
		autocmd BufWinEnter * if (g:lists_modifiable && WindowIsList()) | set modifiable | endif
		autocmd TextChanged * if (g:lists_modifiable && WindowIsList()) | call UpdateList() | endif
		autocmd BufEnter *.py call Python_BufEnter()
		autocmd User CocLocationsChange call setloclist(0, g:coc_jump_locations) | lwindow
		autocmd BufEnter * let b:coc_suggest_blacklist = g:coc_suggest_blacklist
		autocmd VimEnter * call VerifyParsers()
		autocmd User CocNvimInit call VerifyCocExtensions()
		for parser in g:required_parsers
			exe "autocmd FileWritePost " .. parser .. " call Reparse()"
		endfor
		autocmd User CocOpenFloat call setwinvar(g:coc_last_float_win, "&winblend", g:popup_transparency)
	augroup END
endif
