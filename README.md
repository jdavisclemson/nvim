This is my personal neovim configuration.
To use it, you'll need to do the following:

1. Ensure that your python environment is not externally managed.  Note that this can be circumvented, if you independently ensure that the python packages that are required get installed.
2. Clone this repo to ~/.config/nvim (Be sure to back up any existing configuration, if you have one.).
3. Start neovim. More likely than not, it will inform you of missing shell commands and the configuration will not load.
4. You will need to install those shell commands and start it again.  In the event there is a shell command you can't get working, 
     you can remove it from the required_shell_commands variable at the top of ~/.config/nvim/dependencies/dependencies_init.vim.
     If you have to do this, any features that rely on that shell command will not work.  If codelldb (disabled by default in later commits) was the issue, then C++ debugging will not work.
5. Once you stop getting the errors for missing shell commands, you will probably receive a few other prompts to install missing dependencies.
6. If you want things to work as intended, allow it to install everything it prompts you for.
     Note that the coc extensions can take a long time to install (I'd give it about 30 minutes before suspecting a freeze.).
     The coc extensions don't do a good job of displaying installation status, so you will think it's frozen.
7. Also note that due to some unavoidable asynchronous behavior in the autocmds that trigger some of these messages, you may answer yes to everything, 
     restart, and then have it prompt you for something else.  In this case, just answer yes again.  It should be ready to go after this.  Restart neovim to confirm.
