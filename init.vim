
let g:rootdir = fnamemodify(resolve(expand('<sfile>:p')), ':h')

let g:stop_execution = 0
function! GuardedExecution(script_path)
	if !g:stop_execution
		execute "source " .. a:script_path
	endif
endfunction

call GuardedExecution(g:rootdir .. "/dependencies/dependencies_init.vim")
call GuardedExecution(g:rootdir .. "/setups/setups_init.vim")
call GuardedExecution(g:rootdir .. "/functions/functions_init.vim")
call GuardedExecution(g:rootdir .. "/settings/settings_init.vim")
call GuardedExecution(g:rootdir .. "/mappings/mappings_init.vim")
call GuardedExecution(g:rootdir .. "/abbreviations/abbreviations_init.vim")
call GuardedExecution(g:rootdir .. "/autocmds/autocmds_init.vim")

let local_script_path = g:rootdir .. "/local.vim"
if filereadable(local_script_path)
	call GuardedExecution(local_script_path)
endif

