
function! Reparse()
lua << EOF
	local parser = vim.treesitter.get_parser()
	parser.parse()
EOF
endfunction

function! StoreCurrentBuffer()
	let g:debugging_buf = bufnr("%")
endfunction

function! SwitchToStoredBuffer()
	exe "buffer " . g:debugging_buf
endfunction

"function! NormalEnter() abort
"	let l:colnum = col(".")
"	if l:colnum == 1 || l:colnum == col("$")
"		return "\<CR>"
"	else
"		return "\<ESC>lDo\<ESC>p0i"
"	endif
"endfunction

function! CurrentFullTag() abort
	let l:returnval = tagbar#currenttag('%s', '', 'f', g:tagbar_highlight_method)
	return l:returnval
endfunction

function! CurrentTagType() abort
	let l:returnval = tagbar#currenttagtype('%s', '')
	return l:returnval
endfunction

function! TagInformation() abort
	let l:tag = CurrentFullTag()
	let l:type = CurrentTagType()
	let l:ignore_tag_types = ['', 'variable', 'map', 'autocommand group']
	let l:returnval = ""
	if index(l:ignore_tag_types, tolower(trim(l:type))) == -1
		let l:returnval = l:returnval . l:tag
	endif
	return l:returnval
endfunction

function! CloseParentheses()
	let l:needs_closed = 0
	let l:preceeding_chars = getline('.')[0 : col('.') - 2]
	let l:num_open = count(l:preceeding_chars, '(')
	let l:num_close = count(l:preceeding_chars, ')')
	if (l:num_open - l:num_close)
		let l:needs_closed = 1
	endif
	let l:next_char = getline('.')[col('.') - 1 : col('.') - 1]
	if l:needs_closed && l:next_char == ')'
		return "\<right>"
	else
		return ")"
	endif
endfunction

function! SurroundText()
	"Accept user input.
	let l:user_input = input("What do you want to surround the text with?: ")
	"Make sure they entered something.
	if strlen(l:user_input) == 0
		echom "No text entered. Nothing to do."
		return
	endif
	"Determine from user input what the preceeding and trailing characters will be.
	let l:preceeding_chars = ""
	let l:trailing_chars = ""
	let l:split_points = ["''", "\"\"", "()", "[]", "{}"]
	for split_point in l:split_points
		let l:split_point_pos = stridx(l:user_input, split_point)
		if l:split_point_pos != -1
			let l:preceeding_chars = l:user_input[0 : l:split_point_pos]
			let l:trailing_chars = l:user_input[l:split_point_pos + 1 : -1]
			break
		endif
	endfor
	if strlen(l:preceeding_chars) == 0 || strlen(l:trailing_chars) == 0
		let l:preceeding_chars = l:user_input
		let l:trailing_chars = l:user_input
	endif
	"Swap the old text with the new, wrapped text.
	let l:old_text = GetVisualSection()
	let l:new_text = l:preceeding_chars .. l:old_text .. l:trailing_chars
	exe "norm! gvc" .. l:new_text
endfunction

function! IsWindowNeotree()
	let l:fnam = @%
	let l:index = stridx(l:fnam, "neo-tree filesystem")
	if l:index > -1
		return 1
	else
		return 0
	endif
endfunction

function! OpenAllFiles() range
	for a in range(a:firstline, a:lastline)
		exe "exe " . a
		if a == a:lastline
			continue
		endif
		exe "norm \<cr>:call MoveToEdge(\"h\")\<cr>"
	endfor
	exe "norm \<cr>"
endfunction

function! CommentDuplicate()
	let inicol = col('.')
	silent execute "execute \"norm yypk\""
	silent execute "call ToggleComment()"
	silent execute "execute \"norm j0\""
	if inicol >= 2
		silent execute "execute \"norm " .. (inicol - 1) .. "l\""
	endif
endfunction

function! MoveToEdge(edge)
	let l:curwin = win_getid()
	while 1
		let l:prevwin = l:curwin
		silent execute "wincmd " . a:edge
		let l:curwin = win_getid()
		if l:curwin == l:prevwin
			break
		endif
	endwhile
endfunction

function! CloseWindow()
	let option = input("Close Window (c/o/h/j/k/l): ")
	if option == "c"
		silent execute "wincmd c"
	elseif option == "o"
		silent execute "wincmd o"
	elseif option == "h"
		silent execute "wincmd h \| wincmd c"
	elseif option == "j"
		silent execute "wincmd j \| wincmd c"
	elseif option == "k"
		silent execute "wincmd k \| wincmd c"
	elseif option == "l"
		silent execute "wincmd l \| wincmd c"
	else
		echo ""
		echo "Invalid Option"
		return
	endif
endfunction

function! SearchNext(search_backwards)
	if a:search_backwards
		execute "normal! N"
	else
		execute "normal! n"
	endif
	let inipos = getpos('.')
	execute "normal! zz"
	let afterpos = getpos('.')
	let newpos = [afterpos[0], afterpos[1], inipos[2], afterpos[3]]
	call setpos('.', newpos)
endfunction

function! YankToSystemRegister(calling_mode)
	if a:calling_mode == "x"
		execute "normal! gv\"+y"
	else
		let inipos = getpos('.')
		execute "normal! V\"+y"
		call setpos('.', inipos)
	endif
endfunction

function! AbbrevAllowed()
	if getcmdtype() != ':' || (getcmdline() !~ "^[0-9A-Za-z]*$" && getcmdline() !~ "^\'<,\'>[0-9A-Za-z]")
		return 0
	endif
	return 1
endfunction

function! CreateAbbreviations(abbreviations)
	for lst in a:abbreviations
		execute "cabbrev <expr> " . lst[0] . " AbbrevAllowed() ? \"" . lst[1] . "\" : \"" . lst[0] . "\""
	endfor
endfunction

function! DapClearBreakpoints()
	let inipos = getpos('.')
	exec "lua require'dap'.list_breakpoints()"
	for item in getqflist()
		exec "exe " . item.lnum . " | lua require'dap'.toggle_breakpoint()"
	endfor
	call setpos('.',inipos)
endfunction

function! DapSetBreakpoints()
	call DapClearBreakpoints()
	let inipos = getpos('.')
	exec "normal! gg"
	for i in range(1,line('$'))
		exec "lua require'dap'.toggle_breakpoint()"
		exec "normal! j"
	endfor
	call sktpos('.',inipos)
endfunction

function! DapClose()
	exec "lua require'dap'.terminate()"
	exec "lua require'dapui'.close()"
endfunction

function! CleanDapBuffers()
	let l:bufs_to_delete = ['[dap-repl]', 'codelldb_server']
	for buf in getbufinfo()
		if buf.listed && buf.loaded && index(l:bufs_to_delete, bufname(buf.bufnr)) > -1
			execute "Bdelete! " .. buf.bufnr
		endif
	endfor
endfunction

function! GetVisualSection()
	let [l1, c1] = getpos("'<")[1:2]
	let [l2, c2] = getpos("'>")[1:2]
	if l1 == 0
		echo "Error: Selection marks are not set."
		return ""
	endif
	let lines = getline(l1, l2)
	let lines[-1] = lines[-1][: c2 - (&selection == "inclusive" ? 1 : 2)]
	let lines[0] = lines[0][c1 - 1 :]
	return join(lines, "\n")
endfunction

function! FindAll(prompt, search_scope, mode, search_options)
	let text = ""
	if a:mode == "n"
		let text = "/" . input(a:prompt) . a:search_options . "/"
	elseif a:mode == "x"
		let text = "/" . GetVisualSection() . a:search_options . "/"
	else
		echo "Error: Invalid value for mode argument."
		return
	endif
	if trim(text) == "/\\c/"
		echo "Error: Cannot use blank selection."
		return
	endif
	if stridx(text, "\n") != -1
		echo "Error: Cannot use multiline selection."
		return
	endif
	silent! execute "lvimgrep " . text . " " . a:search_scope ." | lopen"
endfunction

function! FindAllInFile(mode, search_options)
	call FindAll("Find in File: ", "%", a:mode, a:search_options)
endfunction

function! FindAllInRoot(mode, search_options)
	call FindAll("Find in Root: ", "**/*", a:mode, a:search_options)
endfunction

function! BreakPoints() range
	let firstline = a:firstline
	let lastline = a:lastline
	if firstline == lastline
		let firstline = 1
		let lastline = line('$')
	endif
	let lines_with_breakpoints = []
	lua require'dap'.list_breakpoints()
	for item in getqflist()
		let lines_with_breakpoints = add(lines_with_breakpoints, item.lnum)
	endfor
	let i1 = index(lines_with_breakpoints,firstline)
	let i2 = index(lines_with_breakpoints,lastline)
	if i1 >= 0 && i2 >= 0 && (i2-i1) == (lastline - firstline)
		let action = "unset"
	else
		let action = "set"
	endif
	let inipos = getpos('.')
	for i in range(firstline, lastline)
		if (action == "set" && index(lines_with_breakpoints, i) < 0) ||
		\ (action == "unset" && index(lines_with_breakpoints, i) >= 0)
			exec "exe " . i . " | lua require'dap'.toggle_breakpoint()"
		endif
	endfor
	call setpos('.',inipos)
endfunction

function! UnComment() range
	"Ensure we know the comment leader.
	if !exists('b:comment_leader')
		echo "Unknown comment leader."
		return
	endif
	"Save the initial cursor position, to restore later.
	let inipos = getpos('.')
	"Loop through the range, uncommenting any commented line.
	for i in range(a:firstline, a:lastline)
		"Move to line i.
		exec "exe " . i
		"Uncomment if commented.
		if getline(i) =~ '\v(\s*|\t*)' . b:comment_leader
			execute 'silent s,\v^(\s*|\t*)\zs' . b:comment_leader . '[ ]?,,g'
		endif
	endfor
	"Restore the initial position.
	call setpos('.', inipos)
endfunction

function! ToggleComment() range
	"Ensure we know the comment leader.
	if !exists('b:comment_leader')
		echo "Unknown comment leader."
		return
	endif
	"Save the initial cursor position, to restore later.
	let l:inipos = getpos('.')
	"Make a list of all of the line numbers in the range which are already commented.
	let l:commented_lines = []
	for i in range(a:firstline, a:lastline)
		if getline(i) =~ '^\s*' . b:comment_leader
			let l:commented_lines = add(l:commented_lines, i)
		endif
	endfor
	"If every line in the range is commented, set the action to uncomment.
	"  Otherwise, set it to comment.
	let l:i1 = index(l:commented_lines, a:firstline)
	let l:i2 = index(l:commented_lines, a:lastline)
	if l:i1 >= 0 && l:i2 >= 0 && (l:i2 - l:i1) == (a:lastline - a:firstline)
		let l:action = "uncomment"
	else
		let l:action = "comment"
	endif
	"Loop through the range, commenting or uncommenting based on l:action.
	for i in range(a:firstline, a:lastline)
		"Move to line i.
		exec "exe " . i
		"Perform the action.
		if l:action == "comment"
			exec 'normal! 0i' . b:comment_leader
		else
			execute 'silent s,' . b:comment_leader . ',,'
		endif
	endfor
	"Restore the initial position.
	call setpos('.', l:inipos)
endfunction

function! Python_BufEnter()
"	Below is the default value for b:SimpylFold_def_re.
	let b:SimpylFold_def_re = "\\v^\\s*%(class|%(async\\s+)?def)\\s+\\w+|if\\s+__name__\\s*\\=\\=\\s*%(\"__main__\"|'__main__')\\s*:"
	let b:local_cabbrevs = [
		\ ["testmethod", "lua require(\'dap-python\').test_method()"],
		\ ["testfunction", "lua require(\'dap-python\').test_method()"],
		\ ["testclass", "lua require(\'dap-python\').test_class()"],
		\ ["testselection", "lua require(\'dap-python\').debug_selection()"],
	\ ]
	silent call CreateAbbreviations(b:local_cabbrevs)
endfunction

function! AddLocationToQuickfixList()
	let l:current_location = {'pattern': '', 'valid': 1, 'vcol': 0, 'nr': 0, 'module': '', 'type': ''}
	let l:current_location['bufnr'] = bufnr('%')
	let l:current_location['lnum'] = line('.')
	let l:current_location['end_lnum'] = line('.')
	let l:current_location['col'] = 1
	let l:current_location['end_col'] = len(getline('.')) + 1
	let l:current_location['text'] = trim(getline('.'))
	let l:qflist = getqflist()
	if index(l:qflist, l:current_location) == -1
		copen
		call add(l:qflist, l:current_location)
		call setqflist(l:qflist, 'r')
	endif
endfunction

function! WindowIsList()
	if WindowIsLocationList() || WindowIsQuickfixList()
		return 1
	endif
	return 0
endfunction

function! WindowIsLocationList()
	let l:qfvar = getwininfo(win_getid())[0]['quickfix']
	let l:llvar = getwininfo(win_getid())[0]['loclist']
	if l:llvar == 1 && l:qfvar == 1
		return 1
	endif
	return 0
endfunction

function! WindowIsQuickfixList()
	let l:qfvar = getwininfo(win_getid())[0]['quickfix']
	let l:llvar = getwininfo(win_getid())[0]['loclist']
	if l:llvar == 0 && l:qfvar == 1
		return 1
	endif
	return 0
endfunction

function! UpdateList()
	"Determine whether or not the current list is a location list or quickfix list.
	"  In the event it is neither, return.
	if WindowIsLocationList()
		let l:list_type = "location"
	elseif WindowIsQuickfixList()
		let l:list_type = "quickfix"
	else
		return
	endif
	"Store the line number of the cursor.
	"  The list will have to be reloaded, which moves the cursor to the top.
	"  This will be used to restore its location.
	let l:iniline = line('.')
	"Get the current contents of the list.
	"  Note that this will include any deleted lines.
	"  When a line is deleted from a list, only the window text is updated by default.
	if l:list_type == "location"
		let l:old_list = getloclist(0)
	elseif l:list_type == "quickfix"
		let l:old_list = getqflist()
	endif
	"Get a list of all the lines visible in the list window.
	"  Note that this list will NOT include any deleted lines.
	"  This will be used to filter the list contents.
	let l:window_text = getline(1, '$')
	"Create an empty list to populate with list items.
	"  Any item in the old_list that gets to stay will be added to this list.
	let l:new_list = []
	"Loop through the old_list.
	for l:list_item in l:old_list
		"Using the list_item, produce its corresponding display text (what the user sees in the list window).
		let l:buffer_name = expand("#" .. l:list_item['bufnr'])
		if l:list_item['end_lnum'] == l:list_item['lnum']
			let l:line_numbers = l:list_item['lnum']
		else
			let l:line_numbers = l:list_item['lnum'] .. "-" .. l:list_item['end_lnum']
		endif
		let l:column_numbers = l:list_item['col'] .. "-" .. l:list_item['end_col']
		let l:preview_text = trim(l:list_item['text'])
		let l:list_item_display_text = l:buffer_name .. "|" .. l:line_numbers .. " col " .. l:column_numbers .. "| " .. l:preview_text
		"If the list item's display text is found in the current window text (ie, it has not been deleted by the user), add the list_item to new_list.
		if index(l:window_text, l:list_item_display_text) != -1
			call add(l:new_list, l:list_item)
		endif
	endfor
	"Update the contents of the list.
	if l:list_type == "location"
		call setloclist(0, l:new_list, 'r')
	elseif l:list_type == "quickfix"
		call setqflist(l:new_list, 'r')
	endif
	"Restore the cursor's line number.
	exe "exe " .. l:iniline
	"Make sure the list is modifiable.
	set modifiable
endfunction
"
function! SearchAndReplace()
	redir => l:raw_input_history
		silent exe "norm! :history input\<cr>"
	redir end
	let l:raw_input_history_list = split(l:raw_input_history, '\n')
	let l:last_raw_input = l:raw_input_history_list[-1]
	let l:last_input = split(l:last_raw_input, ' ')[-1]
	let l:search_for = input("Search for: ", l:last_input)
	let l:replace_with = input("Replace with: ")
	let l:confirmation = input("Replace '" .. l:search_for .. "' with '" .. l:replace_with .. "'? (y/N): ")
	if tolower(l:confirmation) == 'y'
		exe "norm! :ldo | s/" .. l:search_for .. "/" .. l:replace_with .. "/g\<cr>"
		lclose
	endif
endfunction

function! CloseAll()
  :silent bufdo | Bdelete!
endfunction

